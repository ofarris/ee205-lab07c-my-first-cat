/////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
//
// @file    hello2.cpp
// @version 1.0 - Initial implementation
//
// Hello World program 2
//
// @author  Oze Farris <ofarris@hawaii.edu>
// @date    01_03_2022
//
/////////////////////////////////////////////////////////////////////////////


#include <iostream>

int main() {
   std::cout << "Hello World!" << std::endl;
   return 0;
}
