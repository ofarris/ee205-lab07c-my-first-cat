/////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
//
// @file    hello3.cpp
// @version 1.0 - Initial implementation
//
// Prints Meow using a class and an object.
//
// @author  Oze Farris <ofarris@hawaii.edu>
// @date    01_03_2022
//
/////////////////////////////////////////////////////////////////////////////


#include <iostream>
using namespace std;

class Cat {
public:
   void sayHello() {
      cout << "Meow" << endl;
   }
};

int main() {
   Cat myCat;
   myCat.sayHello();
}
